#include "interface_control.h"

#include <LiquidCrystal_I2C.h>
#include "furnace_control.h"
#include "time_control.h"
#include "temperature.h"
#include "burn_prg.h"

interface_control::interface_control(){
    //
    _lcd=nullptr;
    //
    this->_furnace=nullptr;
    this->_timer=nullptr;
    this->_temperature=nullptr;
    this->_program=nullptr;
    _prgName=new char[8];
    //
    pinMode(BTN1_PIN, INPUT_PULLUP);
    pinMode(BTN2_PIN, INPUT_PULLUP);
    pinMode(BTN3_PIN, INPUT_PULLUP);
    //
    _btnData[0].exec=&interface_control::btn1_click;
    _btnData[0].pin=BTN1_PIN;
    _btnData[1].exec=&interface_control::btn2_click;
    _btnData[1].pin=BTN2_PIN;
    _btnData[2].exec=&interface_control::btn3_click;
    _btnData[2].pin=BTN3_PIN;
    //
    _lastLcdRegenTime=0;
    _status=0;
    _selectedPrg=10;
    _lastSelectedPrg=0;
    _initLcd=false;
    //
    _selectedManualTemp=50;
    _lastManualTemp=50;
    //
    memset(_prgName,0,8);
    //
    debug1String="";
    debug2String="";
    //
}

void interface_control::Init(time_control* timer,furnace_control* furnace,temperature* Temperature,burn_prg* program){
    //
    _lcd=new LiquidCrystal_I2C(PCF8574_ADDR_A21_A11_A01, 4, 5, 6, 16, 11, 12, 13, 14, POSITIVE);
    //
    _initLcd=_lcd->begin(20,4);
    delay(100);
    _lcd->backlight();
    _lcd->clear();
    //
    this->_timer=timer;
    this->_furnace=furnace;
    this->_temperature=Temperature;
    this->_program=program;
    //
    _lastLcdRegenTime=0;
    _status=0;
    _selectedPrg=1;
    _lastSelectedPrg=0;
    //
    _selectedManualTemp=50;
    _lastManualTemp=0;
    //
  //
}

void interface_control::Tick(unsigned long ms){
    //

    if(!_initLcd){							
      _initLcd=_lcd->begin(20,4);
      return;
    }
    //
    for(int i=0;i<3;++i){							//обрабатываем нажатие кнопок
        bool btnval=!digitalRead(_btnData[i].pin);	//проверяем состояние кнопки
        //
        if(!_btnData[i].deb_btn && _btnData[i].btnState!=btnval){//если состояние кнопки изменилось
           //													//и нет флага обработки дребезга
            _btnData[i].btndebTime=ms;							//делаем паузу в обработке нажатия на эту кнопку
            _btnData[i].deb_btn=true;							//для исключения дребезга
            //
        }else if((_btnData[i].deb_btn||btnval)&&(ms-_btnData[i].btndebTime)>=BTN_DEB_TIME){	//выполняется после времени дребезга при нажатой кнопке постоянно 
																							//или  только один раз при отпускании
            
            auto t=_btnData[i];		
            (this->*_btnData[i].exec)(ms,btnval,&t);			//вызываем обработчик нажатия кнопки
            _btnData[i].btnState=btnval;								//меняем запомненное состояние кнопки
            _btnData[i].deb_btn=false;
            //
        }
    }
    switch (_status) {					//в зависимости от состояния программы
										//заполняем LCD экран
    case 0:
        if(ms-_lastLcdRegenTime>LCD_TIME_REGEN){
			PrintMenuToLCD();
			_lastLcdRegenTime=ms;
		}
        break;
    case 1:
        if(ms-_lastLcdRegenTime>LCD_TIME_REGEN){
        	PrintWorkToLCD();
			_lastLcdRegenTime=ms;
		}
        break;
    case 2:
        if(ms-_lastLcdRegenTime>LCD_TIME_REGEN){
        	PrintManualtMenuToLCD();
			_lastLcdRegenTime=ms;
		}
		break;
    }
    //
}

void interface_control::PrintWorkToLCD(){
    //
    auto prgTime=_timer->Get_v_prgTime();
    auto stepTime=_timer->Get_v_stepTime();
    _lcd->setCursor(0,0);
    _lcd->printf("Time %02d:%02d:%02d   ",prgTime.hours,prgTime.minutes,prgTime.seconds);
    _lcd->setCursor(0,1);
    _lcd->printf("Tmp:%4d   ",_temperature->ReadCelsius());
    _lcd->setCursor(9,1);
    _lcd->print("Err:");
    auto furErr=_furnace->Errors();
    if(furErr->sTemp){  
        _lcd->setCursor(13,1);
        _lcd->print("sT ");
    }else if(furErr->overTemp){
        _lcd->setCursor(13,1);
        _lcd->print("oT ");
    }else if(furErr->Temp){
        _lcd->setCursor(13,1);
        _lcd->print("dT ");
    }else{
        _lcd->setCursor(13,1);
        _lcd->print("   ");
    }
    //
    DebugShow();
    //
}

void interface_control::PrintMenuToLCD(){
    //
    _lcd->setCursor(0,0);
    _lcd->printf("Select programm:");
    _lcd->setCursor(0,1);
//  if(_selectedPrg==1)
//    _lcd->printf("Chern   ");
//  else if (_selectedPrg==2)
//    _lcd->printf("Glaszur ");
//  else if (_selectedPrg==10)
//    _lcd->printf("Manual  ");
//  else
//    _lcd->printf("        ");
    _lcd->printf(_prgName);
    _lcd->setCursor(9,1);
    _lcd->printf("Err:");
    auto furErr=_furnace->Errors();
    if(furErr->sTemp){
        _lcd->setCursor(13,1);
        _lcd->print("sT ");
    }else if(furErr->overTemp){
        _lcd->setCursor(13,1);
        _lcd->print("oT ");
    }else if(furErr->Temp){
        _lcd->setCursor(13,1);
        _lcd->print("dT ");
    }else{
        _lcd->setCursor(13,1);
        _lcd->print("   ");
    }
  //
    DebugShow();
  //
}

void interface_control::PrintManualtMenuToLCD(){
  _lcd->setCursor(0,0);
  _lcd->printf("%-16s","Select temp:");
  _lcd->setCursor(0,1);
  _lcd->printf("%4d     ",_selectedManualTemp);
  _lcd->setCursor(9,1);
  _lcd->printf("Err:");
  auto furErr=_furnace->Errors();
  if(furErr->sTemp){
    _lcd->setCursor(13,1);
    _lcd->print("sT ");
  }else if(furErr->overTemp){
      _lcd->setCursor(13,1);
      _lcd->print("oT ");
  }else if(furErr->Temp){
      _lcd->setCursor(13,1);
      _lcd->print("dT ");
  }else{
    _lcd->setCursor(13,1);
    _lcd->print("   ");
  }
  //
  DebugShow();
  //
}

void interface_control::btn1_click(unsigned long ms, bool state, interface_control::btnData* data)
{
    //
    if(state){					//при нажатой кнопке
        //
        switch (_status)		//проверим режим работы
        {
        case 0:					//активен экран выбора программы
            if(data->btnState!=state){					//если это первичное нажатие
                //
                if(_selectedPrg==_lastSelectedPrg){		//если программа не изменена
                    _status=1;							//перейти в рабочий режим
                    _furnace->SetPause(false);			//включить печь
                }else{									//если программа изменена
                    if(_selectedPrg==10){				//если выбран ручной режим
                        //
						_lastSelectedPrg=_selectedPrg;	//запомнить программу
														//переключить печь в ручной режим
                        _furnace->SetManualMode(_selectedManualTemp);	//передать выбранную температуру
                        _furnace->SetPause(false);						//включить печь
                        _status=1;										//экран в рабочий режим
                        //
                    }else{         
                        if(_program->Init(_selectedPrg)){	//если новая программа прочитана
                            //
                            _lastSelectedPrg=_selectedPrg;	//запомнить программу
                            _furnace->SetPrg(_selectedPrg);	//печь в режим программы
                            _furnace->SetPause(false);		//включить печь
                            _status=1;						//экран в рабочий режим
                            //
                        }else{								//если не загружена программа
                            _status=0;						//остаться в меню выбора программы
                        }
                    }
                }                
                //
            }
            break;
        case 1:									//активен рабочий экран
            if(data->btnState!=state){			//если это первичное нажатие
				if(_selectedPrg==10){
    	            _status=2;					//экран в режим выбора температуры
	                data->btnlastTime=ms+3100;	//добавить задержку в 3100 мс
				}else{
					_status=0;					//экран в режим выбора прграммы
				}
              	_furnace->SetPause(true);	//отключить печь
            }
            break;
        case 2:									//активен экран выбора температуры
            if(data->btnState!=state){			//если это первичное нажатие
                //
                data->btnlastTime=ms+3100;		//добавить задержку в 3100 мс
                //								//если температура изменена и печь в ручном режиме
                if(_selectedManualTemp!=_lastManualTemp&&_furnace->GetManualMode()){
                    _furnace->SetManualMode(_selectedManualTemp);	//передать выбранную температуру
                    _furnace->SetPause(false);						//включить печь
                    _status=1;										//экран в рабочий режим
                }else{									//если температура не менялась
                    _furnace->SetPause(false);			//включить печь
                    _status=1;							//экран в рабочий режим
                }
                _lastManualTemp=_selectedManualTemp;	//запомнить температуру
            }else{								//если кнопка зажата
                //
                if(ms>data->btnlastTime){		//и прошло время задержки обработки
                    _selectedPrg=_lastSelectedPrg;	//отменить выбор программы
                    _status=0;					//экран в режим выбора прграммы
                }
                //
            }
            break;        
        }
        //
    }
    //
}

void interface_control::btn2_click(unsigned long ms, bool state, interface_control::btnData* data)
{
    //
    if(state){							//если кнопка нажата
        if(data->btnState!=state){		//если это первое нажатие
            data->btnlastTime=ms+1500;	//сделать задержку обработки кнопки в 1500 мс
            ChangeValues(-1);			//и выбрать предидущий пункт в меню
        }else if(data->btnlastTime<ms){	//если задержка прошла и это удержание кнопки
            data->btnlastTime=ms+100;	//выставляем задержку в 100 мс
            ChangeValues(-1);			//и выбираем предидущий пункт в меню
        }
    }
    //
}

void interface_control::btn3_click(unsigned long ms, bool state, interface_control::btnData* data)
{
    //
    if(state){							//если кнопка нажата
        if(data->btnState!=state){		//если это первое нажатие
            data->btnlastTime=ms+1500;	//сделать задержку обработки кнопки в 1500 мс
            ChangeValues(1);			//и выбрать следующий пункт в меню
        }else if(data->btnlastTime<ms){	//если задержка прошла и это удержание кнопки
            data->btnlastTime=ms+100;	//выставляем задержку в 100 мс
            ChangeValues(1);			//и выбираем следующий пункт в меню
        }
    }
    //
}

void interface_control::ChangeValues(int8_t dir){
	//
    if(_status==0){					//если режим работы - выбор программы
        _selectedPrg+=dir;			//изменяем текущую программу
        if(_selectedPrg>10){		//программ не может быть больше 10
            _selectedPrg=1;
        }else if(_selectedPrg<1){	//и меньше 1
            _selectedPrg=10;
        }else if (_selectedPrg==3){	//
            _selectedPrg=10;		//	TODO: Сделать нормальный выбор программ если их
        }else if (_selectedPrg==9){	//	меньше 9
            _selectedPrg=2;			//
        }
		ReadPrgName(_selectedPrg);
    }else if (_status==2){			//для режима выбора температуры изменяем текущую температуру
        _selectedManualTemp+=dir;
        if(_selectedManualTemp>1150){_selectedManualTemp=1150;}
        else if (_selectedManualTemp<50){_selectedManualTemp=50;}
    }
}

void interface_control::ReadPrgName(uint8_t prgNum)
{
    //
    if(prgNum==10)
        memcpy((void*)_prgName,&"Manual  ",8);
    else
        _prgName=_program->GetPrgName(prgNum);
    //
}

inline void interface_control::DebugShow(){
    //
    _lcd->setCursor(0,2);
    _lcd->print(debug1String);
    _lcd->setCursor(0,3);
    _lcd->print(debug2String);
    //
}
