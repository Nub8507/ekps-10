#include "furnace_control.h"

#include "time_control.h"
#include "temperature.h"
#include "burn_prg.h"
#include "interface_control.h"

furnace_control::furnace_control(){
    //
    _err.overTemp=0; 
    _err.sTemp=0;
    _err.Temp=0;
    //
    _temperature=nullptr;
    _program=nullptr;
    _timer=nullptr;
    _interface=nullptr;
    //
    pinMode(HOT_PIN,OUTPUT);
    pinMode(ALARM_PIN,OUTPUT);
    //
    _status=PrgNotSet;
    _needHot=false;
    _needAlarm=true;
    _Vn=0;
    _Vo=0;
    _dT=0;
    _testState=0;
    _testTemp=0;
    _testTime=0;
    _lastCallTime=0;
    _currStepTime=0;
    _Vstep=0;
    _Tstep=0;
    _prgTimeN=0;
    _prgTimeO=0;
    _manualMode=false;
    _manualTemp=0;
    _lastTempTest=0;
    _hotPinValue=false;
    _alarmPinValue=true;
    _kRealV=1;
    //
    _pause=true;
    //
}

void furnace_control::Init(temperature* Temperature,burn_prg* program,time_control* timer,interface_control* iface){
    //
    _temperature=Temperature;
    _program=program;
    _timer=timer;
    _interface=iface;
    //
    _pause=true;
    //
}

void furnace_control::Tick(unsigned long ms){
    //
    if(_temperature->ReadCelsius()>=MAX_TEMP){		//проверка на перегрев
        _err.overTemp=1;							//если перегрев показать ошибку и отключиться
        _needAlarm=true;
    }else{
        _err.overTemp=0;
        _needAlarm=false;
    }
    //
    SetAlarm();
    //
    this->SetTempError(_temperature->TempError());          //показать ошибку датчика температуры
    //
    if(_pause){                                             //если печь в режиме паузы
        //
        _lastCallTime=ms;                                   //не считать время работы
        return;
        //
    }
    //
    CalkRealV(ms);						//рассчитать реальную скорость нагрева/остывания
    //    
    switch (_status){
    case PrgNotSet:						//если программа на загружена ничего не делаем
        break;
    case Test:							//получаем данные для расчетов
        TestSpeed(ms-_lastCallTime);
        break;
    case Change:						//изменяем температуру
        if(ExecPrgChange(ms-_lastCallTime)){
          CheckStepExec();
        }
        break;
    case Maintenance:					//поддерживаем температуру
        ExecPrgMaintenance(ms-_lastCallTime);
        break;
    case Manual:						//ручной режим
        ExecManualPrg(ms-_lastCallTime);
        break;
    }
    //
    if(_err.Temp==1)
        _needHot=0;
    //
    SetHot();							//вкл/откл нагрева
    //
    _lastCallTime=ms;
    //
}

furnace_control::furnaceErr* furnace_control::Errors(){
    return &_err;
}

//планировалось измерение скрости нагрева/охлаждения,
// но потом алгоритм поменялся, а функция осталась
void furnace_control::TestSpeed(unsigned long d_ms){
  //
  auto temp=_temperature->ReadCelsius();
  _dT=10;
  _Vo=_program->GetVo(temp);				//получить из eeprom скорость остывания
  _Vn=_program->GetVn(temp);				//получить из eeprom скорость нагрева
  _lastTempTest=temp;     
  _lastRealVTemp=0;							//запомнить при какой температуре получены скорости
  ReadStatusFromPrg();						//получить состояние печки
  //
}

void furnace_control::ReadStatusFromPrg(){
    if(_manualMode){						//ручной режим
        _status=Manual;
        return;
    }
    //
    auto step=_program->GetCurrStep();		//получить состояние программы
    if(step.type==0){						//нет программы
        _status=PrgNotSet;
    }else if(step.type==2){					//поддержка температуры
        _status=Maintenance;
        InitPrgStep();
    }else{
        _status=Change;						//нагрев
        InitPrgStep();
    }
    //
}

void furnace_control::SetHot(){
  if(_needHot!=_hotPinValue){			//если нужно изменить состояние
      if(_needHot)						//выхода управления нагревом
        digitalWrite(HOT_PIN,HIGH);
      else
        digitalWrite(HOT_PIN,LOW);
      _hotPinValue=_needHot;
  }
}

void furnace_control::SetAlarm(){
  if(_needAlarm!=_alarmPinValue){		//если нужно изменить состояние
      if(_needAlarm)					//выхода аварийного отключения
        digitalWrite(ALARM_PIN,LOW);
      else
        digitalWrite(ALARM_PIN,HIGH);
      _alarmPinValue=_needAlarm;
  }
}

//инициализация начальных значений для нового шага программы
void furnace_control::InitPrgStep(){
    //
    auto prgStep=_program->GetCurrStep();
    //
    _prgTimeN=0;
    _prgTimeO=0;
    _Tstep=prgStep.temperatura;
    _Vstep=prgStep.V_temp;
    if(_Vstep>_Vn)				//нельзя нагревать быстрее максимальной скорости
      _Vstep=_Vn;				//для этой температуры
    //
}

//проверить состояние программы для выбранного шага и температуры
void furnace_control::CheckStepExec(){
    //
    if(_temperature->ReadCelsius()>_lastTempTest+15			//каждые 15 градусов проверять состояние программы
        	||_temperature->ReadCelsius()<_lastTempTest-15){
        _status=Test;
        _testState=0;
        return;
    }
    //
    if(_manualMode){
        _status=Manual;
        _currStepTime=0;
        return;
    }
    //
    auto prgStep=_program->GetCurrStep();
    //
//    Serial.println(freeRam());
    if(prgStep.type==2){				//если режим поддержки температуры
        //
        auto nstep=_program->GetNextStep();
        if((_temperature->ReadCelsius()>=nstep.temperatura)||_currStepTime>=((unsigned long)prgStep.time*1000)){	//если температура больше температуры
            _currStepTime=0;																						//следующего шага, или время прогрева прошло
            auto step=_program->FinishCurrStep();																	//переходим на следующий шаг
            _lastRealVTemp=0;
            if(step.type==0)
              _status=PrgNotSet;            
            else{
              _status=Test;
              _testState=0;
              _timer->StartNextStep((unsigned long)step.time*1000);
            }
        }
        //
    }else if(prgStep.type==1){											//если режим нагрева
        //
        if(_temperature->ReadCelsius()>=((int16_t)prgStep.temperatura)){		//если температурав больше требуемой
            _currStepTime=0;													//перейти к следующему шагу
            auto step=_program->FinishCurrStep();
            _lastRealVTemp=0;
            if(step.type==0)
              _status=PrgNotSet;            
            else{
              _status=Test;
              _testState=0;
              _timer->StartNextStep((unsigned long)step.time*1000);
            }
        }
        //
    }else if(prgStep.type==3){											//если режим остывания
        //
        if(_temperature->ReadCelsius()<=((int16_t)prgStep.temperatura)){		//если температурав меньше требуемой
            _currStepTime=0;													//перейти к следующему шагу
            auto step=_program->FinishCurrStep();
            _lastRealVTemp=0;
            if(step.type==0)
              _status=PrgNotSet;            
            else{
              _status=Test;
              _testState=0;
              _timer->StartNextStep((unsigned long)step.time*1000);
            }
        }
        //
    }
    //
}

void furnace_control::SetPrg(uint8_t prgNum){
    _currStepTime=0;
    _manualMode=false;
    _status=Test;
    _testState=0;
    _timer->StartNewPrg();
    _lastRealVTemp=0;
}

//выполняет рамсчет ширины импульсов для нагрева/остывания
//возвращает false если период регулирования не завершен
//и true при завершении приода регулирования
bool furnace_control::ExecPrgChange(unsigned long ms){
  //
  if(_prgTimeN==0&&_prgTimeO==0){				//вычислим время вкл и выкл печи
    //
    if(_Vn==_Vstep){							//если максимальная скорость нагрева
        _prgTimeN=TIME_PERIOD;					//печь всегда включена
        _prgTimeO=0;
    }else{
        auto k=(float)(_Vstep+_Vo)/(_Vn-_Vstep);
        k*=_kRealV;										//рассчитываем коэффициент с поправкой на реально измеренный коэффициент нагрква
        _prgTimeN=(unsigned long)TIME_PERIOD*(k/(k+1));	//получаем длительности включенного и выключенного состояния
        _prgTimeO=(unsigned long)TIME_PERIOD/(k+1);
    }
    if(_prgTimeN<TIME_MIN){								//если длительность вкл состояния меньше мин.
        //												//увеличиваем период 
        auto k=(float)_prgTimeN/TIME_MIN;
        _prgTimeN=TIME_MIN;
        _prgTimeO=(unsigned long)_prgTimeO/k;
        //
    }
    if(_prgTimeO<200){									//если длительность откл состояния меньше 200 мс
      _prgTimeO=0;										//игнорируем отключение
      _prgTimeN=TIME_PERIOD;
    }else if(_prgTimeO<TIME_MIN){						//если меньше мин увеличиваем длительность периода
      //
      auto k=(float)_prgTimeO/TIME_MIN;
      _prgTimeO=TIME_MIN;
      _prgTimeN=(unsigned long)_prgTimeN/k;
      //
    }
    //
    return false;
  }
  //
  if(_prgTimeN>0){										//если еще осталось время нагрева
    _needHot=true;										//вкл нагрев и уменьшаем длительность нагрева на время с прошлого вызова
    if(ms>_prgTimeN)
      _prgTimeN=0;
    else
      _prgTimeN-=ms;
    return false;
  }
  //
  if(_prgTimeO>0){										//аналдогично нагреву для времени отключения нагрева
    _needHot=false;
    if(ms>_prgTimeO){
      _prgTimeO=0;
      return true;
    }
    else
      _prgTimeO-=ms;
    return false;
  }
  //
  return false;
  //
}

//выполняется при режиме поддержки температуры, также используется для ручного режима
void furnace_control::ExecPrgMaintenance(unsigned long ms){
    //
    if(_temperature->ReadCelsius()<=_Tstep-_dT*2){			//если до температуры стабилизации далеко
      _Vstep=V_MAX;											//нагреваем быстро
      ExecPrgChange(ms);
    }else if(_temperature->ReadCelsius()<=_Tstep-_dT/2){	//если температура стабилизации близко
      _Vstep=V_MAX/2;										//нагреваем медленнее
      ExecPrgChange(ms);
      _currStepTime+=ms;
    }else {													//иначе отключаем нагрев
      _needHot=false;
      _currStepTime+=ms;
    }
    CheckStepExec();
    //
}

void furnace_control::SetTempError(bool val){
    _err.Temp=val;
}

void furnace_control::ExecManualPrg(unsigned long ms){
    //
    _Tstep=_manualTemp;
    ExecPrgMaintenance(ms);
    //
}

void furnace_control::CalkRealV(unsigned long ms)
{
    //
    if(ms<20000)				//первые 20 секунд температура не определена
      return;					//корректировка не производится
    //
    if(_lastRealVTemp==0){								//начинаем измерение
        _lastRealVTemp=_temperature->ReadCelsius();
        _lastRealVTime=ms;
        _kRealV=1;
    }
    //
    if(_lastRealVTemp+1<=_temperature->ReadCelsius()||_lastRealVTemp-1>=_temperature->ReadCelsius()){	//при изменении температуры на градус
        float RealV=1.*1000*60*60/(ms-_lastRealVTime);													//рассчитываем скорость нагрева
        _kRealV=_kRealV*_Vstep/RealV;																	//и получаем коэффициент реального нагрева относительно теоретичкеского
        if(_kRealV<1)
          _kRealV=1;
        _lastRealVTime=ms;
        _lastRealVTemp=_temperature->ReadCelsius();
    }
    //
}

void furnace_control::SetManualMode(int16_t Temp){
    _currStepTime=0;
    _manualTemp=Temp;
    if(!_manualMode)
        _timer->StartNewPrg();
    _manualMode=true;
    _status=Test;
    _testState=0;
//    setPause(false);
}

void furnace_control::SetPause(bool val){
    _pause=val;
    if(val){
        _needHot=!val;
        SetHot();
    }
}
