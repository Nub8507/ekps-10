#include "Arduino.h"

#include "temperature.h"
#include "interface_control.h"
#include "burn_prg.h"
#include "furnace_control.h"
#include "time_control.h"

//#include <EEPROM.h>

temperature Temperature;
burn_prg program;
time_control timer;
furnace_control furnace;
interface_control interface;

void showEEPROM(){
  //
  return;
  //
  Serial.begin(115200);
  
  for(int i=0;i<240;i=i+2){
    Serial.print("adr(");
    Serial.print(i);
    Serial.print(")= ");
    Serial.print(eeprom_read_word((uint16_t*)i));
    Serial.println(";");
  }
  //
  burn_prg::prg_node t;
  auto _prg_name=new char[9];
  for (size_t j = 0; j < 2; j++)
  { 
    auto startPos=240+j*87;
    Serial.print("prg=");
    Serial.println(eeprom_read_byte((uint8_t*)startPos)&0xf);
    Serial.print("prgName=");
    startPos++;
    eeprom_read_block((void*)_prg_name,(const void*)startPos,8);//читаем название
    Serial.println(_prg_name);
    for (size_t i = 0; i < 13; ++i)
    {
      auto pos=249+87*j+6*i;
      eeprom_read_block(&t,(const void*)pos,6);
      //
      Serial.print("step=");
      Serial.println(i);
      Serial.print("Type=");
      Serial.println(t.type);
      Serial.print("temp=");
      Serial.println(t.temperatura);
      Serial.print("time=");
      Serial.println(t.time);
      Serial.print("V=");
      Serial.println(t.V_temp);
      //
    }
  }
  
  //
}

void writeVsData(unsigned long ms){
  //
  /*
  _lcd->setCursor(0,0);
  _lcd->print("t=");
  _lcd->print(t);
  _lcd->print(" ");
  _lcd->print("tm=");
  _lcd->print(temp);
  _lcd->print(" ");
  _lcd->setCursor(0,1);
  _lcd->print("V=");
  _lcd->print(V);
  _lcd->print(" ");
  _lcd->setCursor(0,2);
  _lcd->print("dT=");
  _lcd->print(ms-dT);  
  _lcd->print(" ");
  _lcd->setCursor(0,3);
  _lcd->print("i=");
  _lcd->print((t/20-1)*2+122*fin);
  _lcd->print(" ");
  _lcd->print(eeprom_read_word((uint16_t*)((t/20-1)*2+122*fin)));
  _lcd->print(" ");  
  if(!fin){
    if(t<40){
      dT=ms;
      return;
    }
    if(t-temp>=20){
      V=20.*60*60*1000/(ms-dT);
      auto pos=(t/20-1)*2;
      eeprom_write_word((uint16_t*)pos,V);
      dT=ms;
      temp=t;
    }
  }else{
    if(40<=t&&t<=1160){
      if(temp-t>=20){
        V=20.*60*60*1000/(ms-dT);
        auto pos=(t/20-1)*2+120;
        eeprom_write_word((uint16_t*)pos,V);
        dT=ms;
        temp=t;
      }
    }
  }
  */
  //  
}

void setup() {
  //
//  showEEPROM();
  //
  Temperature.Init();
  //
  timer.Init();
  program.Init(1);
  interface.Init(&timer,&furnace,&Temperature,&program);
  furnace.Init(&Temperature,&program,&timer,&interface);
  //
}

void loop() {
  //
  unsigned long ms=millis();
  //
  Temperature.Tick(ms);
  timer.Tick(ms);
  interface.Tick(ms);
  furnace.Tick(ms);
  //
}

