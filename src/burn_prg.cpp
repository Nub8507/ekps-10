//#include <EEPROM.h>

#include "burn_prg.h"

///Адрес начала блока программы в EEPROM.
#define EEPROM_PRG_START 240
///Максимальное количество шагов в программе
#define MAX_PRG_STEP_SIZE 13
///Размер длины программы в заголовке в байтах
#define PRG_LENGHT_SIZE 1
///Размер имени программы в байтах
#define PRG_NAME_SIZE 8
///Размер заголовка программы в байтах
#define PRG_HEAD_SIZE (PRG_LENGHT_SIZE+PRG_NAME_SIZE)
///Размер шага программы в байтах
#define PRG_NODE_SIZE sizeof(prg_node)
///Размер блока программы в байтах
#define PRG_SIZE (MAX_PRG_STEP_SIZE*PRG_NODE_SIZE+PRG_HEAD_SIZE)
///Адрес начала блока данных скорости охлаждения
#define EEPROM_VO_START 120
///Диапазон температур, для которого скорость постоянна
#define DELTA_TEMP 20



burn_prg::burn_prg(){
    //
    _prg_size=0;        // размер программы и номер пункта
    _currIndex=0;
    _prg_name=new char*[9];
    for(int i=0;i<9;++i){
        _prg_name[i]=new char[PRG_NAME_SIZE];
        memset(_prg_name[i],0,PRG_NAME_SIZE);
        auto startPos=EEPROM_PRG_START+PRG_SIZE*i+PRG_LENGHT_SIZE;//получаем адрес наpзания программы
        eeprom_read_block((void*)_prg_name[i],(const void*)startPos,PRG_NAME_SIZE);//читаем название
    }
    //
    Private_Init(1);    // при включении выбирается первая  программа
}

burn_prg::burn_prg(uint8_t prgNum){
    //
    _prg_size=0;        // размер программы и номер пункта
    _currIndex=0;
    _prg_name=new char*[9];
    for(int i=0;i<9;++i){
        _prg_name[i]=new char[PRG_NAME_SIZE];
        memset(_prg_name[i],0,PRG_NAME_SIZE);
    }
    //
    Private_Init(prgNum);   
    //
}

burn_prg::prg_node& burn_prg::GetCurrStep(){
    //
    if(_prg_size==0||_currIndex==_prg_size){    //если программа не выбрана или завершена    
        _prg_step={0,0,0,0};                    //текущий пункт пустой
    }
    return _prg_step;                           //возвращаем текущий пункт программы
    //
}

burn_prg::prg_node burn_prg::GetNextStep(){
    //
    if(_prg_size==0||_currIndex+1>=_prg_size){  //если программа не выбрана или завершена
        prg_node t={0,0,0,0};
        return t;                               //возвращает пустой пункт
    }
    auto pos=EEPROM_PRG_START+PRG_SIZE*_prg_num+               //получаем адрес следующего шага программы 
            PRG_HEAD_SIZE+PRG_NODE_SIZE*(_currIndex+1);
    prg_node rez;
    eeprom_read_block(&rez,(const void*)(pos),PRG_NODE_SIZE);
    return rez;
    //
}

burn_prg::prg_node& burn_prg::FinishCurrStep(){
    ++_currIndex;                                                   //увеличиваем номер шага
    if(_currIndex<_prg_size){
        auto pos=EEPROM_PRG_START+PRG_SIZE*_prg_num+               //получаем адрес шага программы 
                PRG_HEAD_SIZE+PRG_NODE_SIZE*_currIndex;
        eeprom_read_block(&_prg_step,(const void*)pos,PRG_NODE_SIZE);
        return _prg_step;
    }else{                                                  // Если шаг последний возсвращаем пустой шаг
        _prg_step={0,0,0,0};
        return _prg_step;
    }

}

bool burn_prg::Init(uint8_t prgNum){
    //
    if(_prg_size>0){
        _prg_size=0;
        _currIndex=0;
    }
    Private_Init(prgNum);       
    return true;
}

burn_prg::prg_node& burn_prg::RestartPrg(){
    _currIndex=0;
    auto pos=(uint16_t)EEPROM_PRG_START+(uint16_t)PRG_SIZE*_prg_num+(uint16_t)PRG_HEAD_SIZE;      //получаем адрес первого шага в программе
    eeprom_read_block(&_prg_step,(const void*)pos,PRG_NODE_SIZE);
    return _prg_step;
}

void burn_prg::Private_Init(uint8_t prgNum){
    //
    _prg_num=prgNum-1;      
    if(prgNum>9||prgNum<1){         //проверяем номер программы
        _prg_size=0;                //если он не попадает в диапазон
        return;                     //программа не загружена
    }
    auto startPos=EEPROM_PRG_START+PRG_SIZE*_prg_num;   //получаем адрес начала программы
    _prg_size=eeprom_read_byte((uint8_t*)startPos)&0xf; //читаем количество шагов в программе
    if(_prg_size>MAX_PRG_STEP_SIZE){                    //если больше чем максимум
        _prg_size=0;                                    //программа неправильная
        return; 
    }
    //
    RestartPrg();
    //
}

uint16_t burn_prg::GetVo(uint16_t temp){
    //адрес данных скорости охлаждения
    uint16_t pos=EEPROM_VO_START+((int16_t)temp/DELTA_TEMP-1)*sizeof(int16_t);
    if(pos<122)                 //если температура выходит за пределы
        return 22;              //измеренных значений
    else if (pos>236)           //получаем скорость для конечной точки измерений
        return 800;
    else{                       //если попали в диапазон измерений
        pos-=sizeof(int16_t);
        //среднее арифметическое ближайших трех измерений
        auto rez=eeprom_read_word((uint16_t*)pos);
        pos+=sizeof(int16_t);
        rez+=eeprom_read_word((uint16_t*)pos);
        pos+=sizeof(int16_t);
        rez+=eeprom_read_word((uint16_t*)pos);
        return rez/3;
    }        
    //
}

uint16_t burn_prg::GetVn(uint16_t temp){
    //
    //адрес данных скорости нагрева
    uint16_t pos=((int16_t)temp/DELTA_TEMP-1)*sizeof(int16_t);
    if(pos<2)                   //если температура выходит за пределы
        return 611;             //измеренных значений
    else if (pos>=116)          //получаем скорость для конечной точки измерений
        return 300;
    else{                       //если попали в диапазон измерений
        pos-=sizeof(int16_t);
        //среднее арифметическое ближайших трех измерений
        auto rez=eeprom_read_word((uint16_t*)pos);
        pos+=sizeof(int16_t);
        rez+=eeprom_read_word((uint16_t*)pos);
        pos+=sizeof(int16_t);
        rez+=eeprom_read_word((uint16_t*)pos);
        return rez/3;
    }    
    //
}
