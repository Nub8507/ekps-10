#include "temperature.h"
//#include "Adafruit_MAX31855.h"
#include "max6675.h"

temperature::temperature(){
    _thermocouple = new MAX6675(SCK_PIN, CS_PIN, DO_PIN);
//    thermocouple = new Adafruit_MAX31855 (SCK_PIN, CS_PIN, DO_PIN);
    _currCelsius=0;
    _lastTemp=0;
    _pos=0;
    _errNum=0;
    memset(_t,0,10*sizeof(int16_t));
}

inline int16_t temperature::GetTemp(){        //получаем пемпературу из библиотеки работы с шилдом
  double ttt=_thermocouple->readCelsius()*2;  //опытным путем получено что измеренное значение нужно удвоить
  if (isnan(ttt))return -255;                 //сигнализируем об ошибке измерения
  return ttt+(ttt/100.);                      //корректировка нелинейности, примерно 1 градус на 100 градусов температуры
}

//Расчитываем среднее для ускорения получения данных температуры
inline int16_t temperature::Prv_ReadCelsius(){
    int16_t tmp=0;
    for(uint8_t i=0;i<_size;++i){
      tmp+=_t[i];
    }
    return tmp/_size;
}

void temperature::Tick(unsigned long ms){
  if(ms>=_lastCallTime+200){              //каждые 200 мс вызывается считывание температуры
    Add(GetTemp());                       
    _lastCallTime=ms;
  }
}

//Добавление измеренной температуры в массив для вычисления среднего.
void temperature::Add(int16_t temp){
    if (temp==-255){              //обработка ошибки измерения температуры
      ++_errNum;                  //если счетчик ошибок >= 20 выводим флаг ошибки
      if(_errNum>20)_errNum=20;   //защита от переполнения
      return;
    }
    //                                        //защита от неправильно измеренных значений
    if(5-_lastTemp<temp&&temp<_lastTemp+5){   // между измерениями не может быть разница больше 5 градусов
      //
      _t[_pos]=temp;                          //если измерение правильное
      ++_pos;                                 //добавляем его в массив
      if(_pos>_size-1)
        _pos=0;
      _errNum=0;
      //
      _currCelsius=Prv_ReadCelsius();         //расчитываем среднее для ускорения получения данных температуры
    }
    _lastTemp=temp;                     //запоминаем последнее измерение для защиты от скачков
}

void temperature::Init(){
  memset(_t,0,10*sizeof(int16_t));
  _pos=0;
  _lastCallTime=0;
  _currCelsius=0;
  _errNum=0;
  _lastTemp=0;
}