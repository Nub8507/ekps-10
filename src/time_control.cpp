#include "time_control.h"

time_control::time_control(){
    _prg_stepTime=0;
    _prgTime=0;
    _lenghtStep=0;
    _prgStart=false;
    _stepStart=false;
    _lastTickTime=0;
}

void time_control::Init(){
    _prg_stepTime=0;
    _prgTime=0;
    _lenghtStep=0;
    _prgStart=false;
    _stepStart=false;
    _lastTickTime=0;
}

void time_control::Tick(unsigned long ms){
    //
    if(_lastTickTime>ms){           //если произошло переполнение переменной пропускаем этот вызов
        _lastTickTime=ms;
        return;
    }
    //
    if(!_prgStart)return;           //если отсчет для программы не запущщен ничего не делаем
    //
    ms-=_lastTickTime;              //теперь ms - время между вызовами!!!!!
    _lastTickTime+=ms;
    _prgTime+=ms;
    _prg_stepTime+=ms;
    if(_lenghtStep!=0){
        if(ms>=_lenghtStep){
            _lenghtStep=0;
        }else{
            _lenghtStep-=ms;
        }
    }
    //
}

void time_control::StartNewPrg(){
    //
    _prgStart=true;
    _stepStart=false;
    _prgTime=0;
    _prg_stepTime=0;
    _lastTickTime=millis();
    //
}

void time_control::StartNextStep(unsigned long lenghtStep){
    //
    if(!_prgStart)return;
    //
    _stepStart=true;
    _lenghtStep=lenghtStep;
    _prg_stepTime=0;
    //
}

time_control::Time time_control::Get_v_stepTime(){
    //
    Time t;
    unsigned long t_time=_prg_stepTime;
    t.hours=t_time/((unsigned long)60*60*1000);
    t_time-=(unsigned long)t.hours*60*60*1000;
    t.minutes=t_time/(60.*1000);
    t_time-=(unsigned long)t.minutes*60*1000;
    t.seconds=t_time/1000;
    //
    return t;
    //
}

time_control::Time time_control::Get_v_prgTime(){
    //
    Time t;
    auto t_time=_prgTime;
    t.hours=1.*t_time/((unsigned long)60*60*1000);
    t_time-=(unsigned long)t.hours*60*60*1000;
    t.minutes=1.*t_time/(60.*1000);
    t_time-=(unsigned long)t.minutes*60*1000;
    t.seconds=t_time/1000;
    //
    return t;
    //
}

void time_control::PrgFinished(){
    //
    _prgStart=false;
    _stepStart=false;
    //
}
