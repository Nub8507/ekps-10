#include <EEPROM.h>

    struct prg_node{
        uint16_t type           :2;             //тип шага 1-разогрев, 2-поддержка, 3-остывание
        uint16_t temperatura    :14;            //температура в градусах
        uint16_t time           :16;            //время в сек
        uint16_t V_temp         :16;            //скорость повышения температуры в град/час
    };
    
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
/*  for(int i=0;i<512;++i){
    Serial.print("eeprom_update_word(");
    Serial.print(2*i);
    Serial.print(",");
    Serial.print(eeprom_read_word(2*i));
    Serial.println(");");
//    Serial.println(EEPROM.read(i));
  }*/
  for(int i=0;i<1024;++i){
    Serial.print("adr(");
    Serial.print(i);
    Serial.print(")= ");
    Serial.print(eeprom_read_byte(i));
    Serial.println(";");
//    Serial.println(EEPROM.read(i));
  }

/*
eeprom_update_word(0,65535);
eeprom_update_word(2,611);
eeprom_update_word(4,611);
eeprom_update_word(6,1100);
eeprom_update_word(8,1356);
eeprom_update_word(10,1585);
eeprom_update_word(12,1679);
eeprom_update_word(14,1682);
eeprom_update_word(16,1685);
eeprom_update_word(18,1735);
eeprom_update_word(20,1791);
eeprom_update_word(22,1798);
eeprom_update_word(24,1798);
eeprom_update_word(26,1771);
eeprom_update_word(28,1771);
eeprom_update_word(30,1818);
eeprom_update_word(32,1788);
eeprom_update_word(34,1811);
eeprom_update_word(36,1742);
eeprom_update_word(38,1760);
eeprom_update_word(40,1712);
eeprom_update_word(42,1677);
eeprom_update_word(44,1680);
eeprom_update_word(46,1589);
eeprom_update_word(48,1547);
eeprom_update_word(50,1567);
eeprom_update_word(52,1463);
eeprom_update_word(54,1456);
eeprom_update_word(56,1387);
eeprom_update_word(58,1321);
eeprom_update_word(60,1323);
eeprom_update_word(62,1252);
eeprom_update_word(64,1219);
eeprom_update_word(66,1132);
eeprom_update_word(68,1073);
eeprom_update_word(70,1069);
eeprom_update_word(72,999);
eeprom_update_word(74,989);
eeprom_update_word(76,921);
eeprom_update_word(78,876);
eeprom_update_word(80,864);
eeprom_update_word(82,782);
eeprom_update_word(84,765);
eeprom_update_word(86,737);
eeprom_update_word(88,707);
eeprom_update_word(90,701);
eeprom_update_word(92,660);
eeprom_update_word(94,625);
eeprom_update_word(96,583);
eeprom_update_word(98,561);
eeprom_update_word(100,540);
eeprom_update_word(102,494);
eeprom_update_word(104,476);
eeprom_update_word(106,431);
eeprom_update_word(108,395);
eeprom_update_word(110,368);
eeprom_update_word(112,337);
eeprom_update_word(114,315);
eeprom_update_word(116,300);
eeprom_update_word(118,300);
eeprom_update_word(120,22);
eeprom_update_word(122,22);
eeprom_update_word(124,22);
eeprom_update_word(126,22);
eeprom_update_word(128,22);
eeprom_update_word(130,28);
eeprom_update_word(132,33);
eeprom_update_word(134,37);
eeprom_update_word(136,43);
eeprom_update_word(138,50);
eeprom_update_word(140,56);
eeprom_update_word(142,63);
eeprom_update_word(144,69);
eeprom_update_word(146,76);
eeprom_update_word(148,86);
eeprom_update_word(150,90);
eeprom_update_word(152,101);
eeprom_update_word(154,106);
eeprom_update_word(156,114);
eeprom_update_word(158,125);
eeprom_update_word(160,130);
eeprom_update_word(162,141);
eeprom_update_word(164,146);
eeprom_update_word(166,154);
eeprom_update_word(168,168);
eeprom_update_word(170,173);
eeprom_update_word(172,187);
eeprom_update_word(174,195);
eeprom_update_word(176,209);
eeprom_update_word(178,222);
eeprom_update_word(180,233);
eeprom_update_word(182,251);
eeprom_update_word(184,253);
eeprom_update_word(186,269);
eeprom_update_word(188,290);
eeprom_update_word(190,298);
eeprom_update_word(192,320);
eeprom_update_word(194,324);
eeprom_update_word(196,340);
eeprom_update_word(198,369);
eeprom_update_word(200,374);
eeprom_update_word(202,402);
eeprom_update_word(204,409);
eeprom_update_word(206,433);
eeprom_update_word(208,464);
eeprom_update_word(210,472);
eeprom_update_word(212,514);
eeprom_update_word(214,522);
eeprom_update_word(216,545);
eeprom_update_word(218,596);
eeprom_update_word(220,612);
eeprom_update_word(222,661);
eeprom_update_word(224,676);
eeprom_update_word(226,711);
eeprom_update_word(228,767);
eeprom_update_word(230,763);
eeprom_update_word(232,767);
eeprom_update_word(234,800);
eeprom_update_word(236,800);
eeprom_update_word(238,800);
eeprom_update_word(1022,65535);
//
prg_node t;
eeprom_update_byte(240,7);
eeprom_update_block(&"Chern   ",241,8);
t={1,100,0,75};
eeprom_update_block(&t,249,6);
t={1,400,0,100};
eeprom_update_block(&t,255,6);
t={2,400,1200,0};
eeprom_update_block(&t,261,6);
t={1,600,0,100};
eeprom_update_block(&t,267,6);
t={2,600,1200,0};
eeprom_update_block(&t,273,6);
t={1,980,0,130};
eeprom_update_block(&t,279,6);
t={2,980,1200,0};
eeprom_update_block(&t,285,6);
//
t={0,0,0,0};
eeprom_update_block(&t,291,6);
eeprom_update_block(&t,297,6);
eeprom_update_block(&t,303,6);
eeprom_update_block(&t,309,6);
eeprom_update_block(&t,315,6);
eeprom_update_block(&t,321,6);
/////////
eeprom_update_byte(327,8);
eeprom_update_block(&"Glaszur ",328,8);
t={1,100,0,75};
eeprom_update_block(&t,336,6);
t={1,400,0,100};
eeprom_update_block(&t,342,6);
t={2,400,1200,0};
eeprom_update_block(&t,348,6);
t={1,600,0,100};
eeprom_update_block(&t,354,6);
t={2,600,1200,0};
eeprom_update_block(&t,360,6);
t={1,800,0,130};
eeprom_update_block(&t,366,6);
t={1,1050,0,100};
eeprom_update_block(&t,372,6);
t={2,1050,1800,0};
eeprom_update_block(&t,378,6);
t={0,0,0,0};
eeprom_update_block(&t,384,6);
eeprom_update_block(&t,390,6);
eeprom_update_block(&t,396,6);
eeprom_update_block(&t,402,6);
eeprom_update_block(&t,408,6);
*/
//
}

void loop() {
  // put your main code here, to run repeatedly:

}
