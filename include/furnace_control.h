#ifndef furnace_control_h
#define furnace_control_h

#include "Arduino.h"

/// пин подключения нагревателя
#define HOT_PIN 7

/// пин подключения аварийного выключателя
#define ALARM_PIN 2

/// минимальное время включения или выключения нагрева в мс
#define TIME_MIN 1000

/// период импульсов ШИМ в мс
#define TIME_PERIOD 10000

/// примерное инерциальное изменение температуры после отключения нагрева/охлаждения
#define DTEMP_MAX 10

/// максимальная допустимая температура
#define MAX_TEMP 1200

/// максимальная скорость нагрева при ручном режиме
#define V_MAX 300

class temperature;
class burn_prg;
class time_control;
class interface_control;
//
/*!
*	\brief Класс управления печью
*   
*   Класс осуществляет непосредственное управление влючением и выклчением печи
*	в соответствии с загруженной программой и текущей температурой.
*
*/
class furnace_control
{	
	///Описание возможный ошибок
	struct furnaceErr{
		uint8_t Temp:1;			///<Неисправен датчик температуры
		uint8_t sTemp:1;		///<(не используется, отладочный режим) температура задана в ручную, датчик не используется
		uint8_t overTemp:1;		///<Перегрев
  	};

	///Состояние печи
	enum furnaceStatus{
		PrgNotSet     = 0,		///<Программа не выбрана
		Test          = 1,		///<Режим определения скорости нагрева/остывания
		Change        = 2,		///<Режим нашрева/остывания
		Maintenance   = 3,		///<Режим поддержки температуры
		Manual        = 9,		///<Ручной режим работы, программа не используется
	};

public:
	//! Конструктор
    furnace_control();

	//! Первоначально заполняет данные для работы
    /*!
        \param Temperature указатель на класс управления дачиком температуры
        \param program указатель на класс управления программой
        \param timer указатель на класс управления временем
        \param iface указатель на класс пользовательского интерфейса
    */
    void Init(temperature* Temperature,burn_prg* program,time_control* timer,interface_control* iface);

    //! Обработчик программного прерывания
    /*!
    *   Обработчик программного прерывания вызывается в основном цикле программы и
    *   отрабатывает изменения произошедшие с момента прошлого запуска.
    *   \param ms количество миллисекунд и начала работы контроллера
    */	
    void Tick(unsigned long ms);

    //! Запускает обжиг по программе
    /*!
    *   \param prgNum номер программмы
    */
    void SetPrg(uint8_t prgNum);

    //! Запускает обжиг при фиксированной температуре
    /*!
	*	Запускает обжиг при фиксированной температуре(ручной режим).
    *   \param Temp температура
    */
    void SetManualMode(int16_t Temp);


    void SetTempError(bool val);

    //! Запускает/останавливает обжиг
    /*!
    *   \param val включена или выключена пауза
    */
    void SetPause(bool val);

    //! Возвращает статус ручного режима
    /*!
    *   \return статус ручного режима
    */
    inline bool GetManualMode();

    furnaceErr* Errors();

	/// \cond
    furnace_control( const furnace_control& );     //Запретили копирование обьекта( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    void operator=( const furnace_control& );      //Запретили присваивание( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    /// \endcond

private:

  furnaceErr _err;
  temperature* _temperature;
  burn_prg* _program;
  time_control* _timer;
  interface_control* _interface;

  furnaceStatus _status;
  bool _needHot;		//флаг необхъодимости включить нагрев
  bool _needAlarm;		//флаг необходимости аварийного прерывания
  uint16_t _Vn;
  uint16_t _Vo;
  int16_t _dT;
  uint8_t _testState;
  int16_t _testTemp;
  unsigned long _testTime;  
  unsigned long _lastCallTime;
  unsigned long _currStepTime;
  uint16_t _Vstep;
  int16_t _Tstep;
  unsigned long _prgTimeN;
  unsigned long _prgTimeO;
  bool _manualMode;
  int16_t _manualTemp;
  int16_t _lastTempTest;
  bool _hotPinValue;
  bool _alarmPinValue;
  bool _pause;

  float _kRealV;
  int16_t _lastRealVTemp;
  unsigned long _lastRealVTime;

  void TestSpeed(unsigned long d_ms);
  void ReadStatusFromPrg();
  void InitPrgStep();
  bool ExecPrgChange(unsigned long d_ms);
  void SetHot();
  void SetAlarm();
  void CheckStepExec();
  void ExecPrgMaintenance(unsigned long d_ms);
  void ExecManualPrg(unsigned long d_ms);
  void CalkRealV(unsigned long ms);
};

inline bool furnace_control::GetManualMode()
{
    return _manualMode;
}

#endif
