#ifndef time_control_h
#define time_control_h

#include "Arduino.h"

/*!
*	\brief Класс времени
*   
*   В этом классе храниться и отслеживается врема потраченное на выполнение всей программы и ее отдельных кусков
*
*/

class time_control
{
    ///Структура для показа времени
    /*!
    *   Структура предназначена для показа времени в удобном для человека виде.
    *   Отдельно храняться часы, минуты и секунды. \n
    *   Часы показываются в 24 часовом формате.
    */
    struct Time{
        uint8_t hours;          ///<Часы
        uint8_t minutes;        ///<Минуты
        uint8_t seconds;        ///<Секунды
    };

public:
    //! Конструктор
    time_control();

    //! Инициализатор класса
    /*!
    *   При запуске прекрасчается отчет времени и значения обнуляются
    */
    void Init();

    //! Обработчик программного прерывания
    /*!
    *   Обработчик программного прерывания вызывается в основном цикле программы и
    *   отрабатывает изменения произошедшие с момента прошлого запуска.
    *   \param ms количество миллисекунд и начала работы контроллера
    */
    void Tick(unsigned long ms);

    //! Запуск отсчета для новой программы
    /*!
    *   Запускает отсчет времени для программы с нуля. Отсчет времени цикла программы выключается.
    */
    void StartNewPrg();

    //! Запуск отсчета для следующего шага программы
    /*!
    *   Запускает отсчет нового шага программы. Общее время программы не сбрасывается.
    *   \param lenghtStep длина шага
    */
    void StartNextStep(unsigned long lenghtStep);

    //! Получить время шага
    /*!
    *   \return время текущего шага
    */
    inline unsigned long GetStepTime();

    //! Получить время программы
    /*!
    *   \return время текущей программы
    */
    inline unsigned long GetPrgTime();

    //! Получить время шага
    /*!
    *   \return время текущего шага в удобном для человека формате \a time_control::Time
    */
    time_control::Time Get_v_stepTime();

    //! Получить время программы
    /*!
    *   \return время текущей программы в удобном для человека формате \a time_control::Time
    */
    time_control::Time Get_v_prgTime();

    //! Завершение отсчета для программы
    /*!
    *   Прекращает отсчет времени программы и шага. Отсчетанное ранее время не сбрасывается
    */   
    void PrgFinished();

    /// \cond
    time_control( const time_control& );            //Запретили копирование обьекта( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    void operator=( const time_control& );      //Запретили присваивание( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    /// \endcond

private:

    unsigned long _prg_stepTime;        //время шага
    unsigned long _prgTime;             //время программы
    unsigned long _lenghtStep;          //оставшаяся длина шага

    bool _prgStart;                     //флаг включения отсчета времени программы
    bool _stepStart;                    //флаг включения отсчета времени шага
    unsigned long _lastTickTime;        

    time_control::Time Sec_To_Time();

};

inline unsigned long time_control::GetStepTime(){
    return _prg_stepTime;
}

inline unsigned long time_control::GetPrgTime(){
    return _prgTime;
}

#endif