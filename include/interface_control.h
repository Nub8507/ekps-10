#ifndef interface_control_h
#define interface_control_h

#include "Arduino.h"

/// пин подключения 1 кнопки
#define BTN1_PIN 12

/// пин подключения 2 кнопки
#define BTN2_PIN 10

/// пин подключения 3 кнопки
#define BTN3_PIN 11

/// время обновления LCD экрана 
#define LCD_TIME_REGEN 200

/// время задержки для исключения дребезга кнопок
#define BTN_DEB_TIME 40

class furnace_control;
class time_control;
class temperature;
class burn_prg;
class LiquidCrystal_I2C;

/*!
*	\brief Класс пользовательского интерфейса
*   
*   Класс обрабатывает нажатие кнопок на панели и отображает данные на LCD экране
*
*/
class interface_control
{

public:

    //! Конструктор
    interface_control();
    
    //! Первоначально заполняет данные для работы
    /*!
    *   \param timer ссылка на класс таймера
    *   \param furnace ссылка на класс печи
    *   \param Temperature ссылка на класс датчика температуры
    *   \param program ссылка на класс уравлеения программой
    */
    void Init(time_control* timer,furnace_control* furnace,temperature* Temperature,burn_prg* program);

    //! Обработчик программного прерывания
    /*!
    *   Обработчик программного прерывания вызывается в основном цикле программы и
    *   отрабатывает изменения произошедшие с момента прошлого запуска.
    *   \param ms количество миллисекунд и начала работы контроллера
    */
    void Tick(unsigned long ms);

	/// \cond
    interface_control( const interface_control& );            //Запретили копирование обьекта( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    void operator=( const interface_control& );      //Запретили присваивание( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    /// \endcond

    /// \cond
    String debug1String;
    String debug2String;
    /// \endcond


private:
	//структура состояния кнопки
    struct btnData{
        using funcPointer = void (interface_control::*)(unsigned long, bool,btnData*); 
        unsigned long btndebTime;	//интервал для исключения дребезга
        unsigned long btnlastTime;	//время 
        funcPointer exec;			//указатель на функцию обработки нажатий
        int8_t pin;					//номер пина
        bool btnState;				//состояние кнопки true - нажата, false - отпущена
        bool deb_btn;				//флаг обработки дребезга

        btnData(){
            btndebTime=0;
            btnlastTime=0;
            exec=nullptr;
            btnState=true;
            deb_btn=false;
            pin=-1;
        }
    };

    LiquidCrystal_I2C* _lcd;
    
    furnace_control* _furnace;
    time_control* _timer;
    temperature* _temperature;
    burn_prg*   _program;

    unsigned long _lastLcdRegenTime;	//прошлый момент обновления LCD
    bool _initLcd;						//флаг включения LCD
    uint8_t _status;         //0 - меню, 1 - работа, 2 - ручной режим
    int8_t _selectedPrg;		//номер выбранной программы
    int8_t _lastSelectedPrg;	//номер программы до начала выбора в меню
    int16_t _selectedManualTemp;//флаг ручного режима работы
    int16_t _lastManualTemp;	//температура ручного режима до начала выбора в меню
    btnData _btnData[3];		//массив состояний кнопок
    char* _prgName;				//название программы

    void PrintWorkToLCD();
    void PrintMenuToLCD();
    void PrintManualtMenuToLCD();
    void btn1_click(unsigned long ms, bool state, interface_control::btnData* data);
    void btn2_click(unsigned long ms, bool state, interface_control::btnData* data);
    void btn3_click(unsigned long ms, bool state, interface_control::btnData* data);

	/// обработка изменений пунктов в меню
	/*!
	*	\param dir - направление изменений, \a 1 - увеличение, \a -1 - уменьшение
	*/
    void ChangeValues(int8_t dir);
    void ReadPrgName(uint8_t prgNum);

    void DebugShow();
};

#endif
