#ifndef temperature_h
#define temperature_h

#include "Arduino.h"

///Номер пина для подключения выхода \a SCK
#define SCK_PIN 3
///Номер пина для подключения выхода \a CS
#define CS_PIN 4
///Номер пина для подключения выхода \a DO
#define DO_PIN 5

//class Adafruit_MAX31855;
class MAX6675;
/*!
*	\brief Класс измерителя температуры
*   
*   Класс предоставляет доступ к показаниям термометры. Для измерения используетсяя шилд 
*   на базе микросхемы \a MAX31855. Но или у меня микросхеми испорчена, или библиотеки кривые,
*   с библиотекой Adafruit_MAX31855 контроллер работать отказался. Была использована библиотека 
*   \a MAX6675. Путем подбора были найдены коэффициенты, позволющие с ее помощью измерять 
*   температуру печи. Для переделки под правильную библиотеку необходимо поправить функцию 
*   \a int16_t \a GetTemp() \n
*   \n
*   Значение измеренной температуры берется скользящим средним за \a _size измерений.
*
*/
class temperature
{
public:
    //! Конструктор
    /*!
    *   Инициализируем библиотеку \a MAX6675 значениями пинов \a SCK_PIN, \a CS_PIN и \a DO_PIN
    */
    temperature();

     ///Количество измерений для вычисления скользящего среднего
    static const uint8_t _size=10;

   //! Сбрасывает показания и начинает новое измерение.
    void Init();

    //! Обработчик программного прерывания
    /*!
    *   Обработчик программного прерывания вызывается в основном цикле программы и
    *   отрабатывает изменения произошедшие с момента прошлого запуска.
    *   \param ms количество миллисекунд и начала работы контроллера
    */
    void Tick(unsigned long ms);

    //! Получение температуры
    /*!
    *   \return среднюю скользящую температуру за последние \a _size измерений
    */
    inline int16_t ReadCelsius();

    //! Ошибка измерений
    /*!
    *   Возщвращает \a true если в течении 20 попыток не получилось измерить температуру.
    *   \return флаг ошибки измерений
    */
    inline bool TempError();

    /// \cond
    temperature( const temperature& );     //Запретили копирование обьекта( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    void operator=( const temperature& );  //Запретили присваивание( реализации не будет, чтоб при попытке это сделать ошибка копмиляции)
    /// \endcond
private:

    /// \cond
    MAX6675* _thermocouple;
    /// \endcond

    /// \cond
    int16_t _t[temperature::_size];
    int16_t _lastTemp;
    int16_t _currCelsius;
    uint8_t _pos;
    int8_t _errNum;
    unsigned long _lastCallTime;    

    void Add(int16_t temp);
    int16_t GetTemp();
    int16_t Prv_ReadCelsius();
    /// \endcond
};

inline int16_t temperature::ReadCelsius(){
    return _currCelsius;
}

inline bool temperature::TempError(){
    if(_errNum>=20)
      return true;
    return false;
}

#endif

